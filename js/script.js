$(".search").click(function(){
  $(".response").val("Please wait...")
  data = {
    "post_url": $(".input-field").val()
  };
  $.ajax({
		url: "https://parse-inst.herokuapp.com/api/location",
    type: "POST",
    contentType:'application/json',
    data: JSON.stringify(data),
    dataType:'json'
  })
  .always((res)=>{
    $(".response").val(JSON.stringify(res, null, 2))
  })
});
